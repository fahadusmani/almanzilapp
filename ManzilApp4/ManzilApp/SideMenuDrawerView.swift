//
//  SideMenuDrawerView.swift
//  ManzilApp
//
//  Created by Admin on 8/1/17.
//  Copyright © 2017 Mujadidia. All rights reserved.
//

import UIKit

class SideMenuDrawerView: UIViewController, UITableViewDataSource, UITableViewDelegate, SideMenuDrawerCellProtocol
{
    
    @IBOutlet weak var sideMenuDrawerTopView: UIView!
    
    @IBOutlet weak var imageVieww: UIImageView!
    @IBOutlet weak var sideMenuDrawerTable: UITableView!
    
    var mainView = MainVc()
    
    var cellClass = SideMenuDrawerTableCell()
    var controllerObject = SideMenuViewController()
    let bgColorView = UIView()
    
    static var sideMenuDrawerClosed = 0
    
    //  var bookViewObj = BookView()
    override func viewDidLoad()
    {
        super.viewDidLoad()
        cellClass.delegate = self
        
        //  sideMenuDrawerBottomView.backgroundColor = Colors.themeColor
        
        //bgColorView.backgroundColor = Colors.cellSelectionColor
    }
    
    
    override func viewWillAppear(_ animated: Bool)
    {
        setViewLayouts()
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        SideMenuDrawerView.sideMenuDrawerClosed = 1
        //  BookView.updateTable = 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return CGFloat(110)
        case 1:
            return CGFloat(110)
        default:
            return CGFloat(70)
        }
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 6//7
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        switch indexPath.row
        {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "audioPlaybackCell", for: indexPath) as! SideMenuDrawerTableCell
            cell.delegate = self
            cell.audioCellImageView.image = #imageLiteral(resourceName: "audio_50")
            cell.audioCellLabel.text = "Audio PlayBack"
            cell.audioCellSegmentController.tintColor = Colors.themeColor
            cell.selectedBackgroundView = bgColorView
            cell.audioCellSegmentController.selectedSegmentIndex = AudioPlayer.audioPlayerFormat
            return cell
            break
            
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "fontSettingCell", for: indexPath) as! SideMenuDrawerTableCell
            cell.delegate = self
            cell.fontCellImageView.image = #imageLiteral(resourceName: "font_50")
            cell.fontCellLabel.text = "Font"
            cell.fontCellSegmentController.tintColor = Colors.themeColor
            
            switch Colors.fontSize  //BookCollectionViewCell.fontSize
            {
            case 32:
                cell.fontCellSegmentController.selectedSegmentIndex = 0
            case 40:
                cell.fontCellSegmentController.selectedSegmentIndex = 1
            case 48:
                cell.fontCellSegmentController.selectedSegmentIndex = 2
            default:
                break
            }
            
            //cell.selectedBackgroundView = bgColorView
            return cell
            
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "drawerCell", for: indexPath) as! SideMenuDrawerTableCell
            cell.delegate = self
            cell.drawerCellImageView.image = #imageLiteral(resourceName: "website")
            cell.drawerCellLabel.text = "About Us"
            cell.selectedBackgroundView = bgColorView
            return cell
            
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: "drawerCell", for: indexPath) as! SideMenuDrawerTableCell
            cell.delegate = self
            cell.drawerCellImageView.image = #imageLiteral(resourceName: "share_50")
            cell.drawerCellLabel.text = "Share with a Friend"
            cell.selectedBackgroundView = bgColorView
            return cell
            
        case 4:
            let cell = tableView.dequeueReusableCell(withIdentifier: "drawerCell", for: indexPath) as! SideMenuDrawerTableCell
            cell.delegate = self
            cell.drawerCellImageView.image = #imageLiteral(resourceName: "rate_50")
            cell.drawerCellLabel.text = "Rate this App"
            cell.selectedBackgroundView = bgColorView
            return cell
            
        case 5:
            let cell = tableView.dequeueReusableCell(withIdentifier: "drawerCell", for: indexPath) as! SideMenuDrawerTableCell
            cell.delegate = self
            cell.drawerCellImageView.image = #imageLiteral(resourceName: "showcase_50")
            cell.drawerCellLabel.text = "App ShowCase"
            cell.selectedBackgroundView = bgColorView
            return cell
            
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "drawerCell", for: indexPath) as! SideMenuDrawerTableCell
            cell.delegate = self
            cell.drawerCellImageView.image = #imageLiteral(resourceName: "buy_50")
            cell.drawerCellLabel.text = "Buy Pro Version"
            cell.selectedBackgroundView = bgColorView
            return cell
        }
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
        
    {
        switch indexPath.row {
            
        case 5:
            
            DispatchQueue.main.async(execute: {
                self.performSegue(withIdentifier: "appShowCase", sender: nil) })
            
        case 2:
            DispatchQueue.main.async(execute: {
                self.performSegue(withIdentifier: "AboutView", sender: nil) })
            
        case 3:
            DispatchQueue.main.async(execute: {
                let shareWidget = self.controllerObject.showShareWidget(key: "Share with a friend")
                self.present(shareWidget, animated: true, completion: {})
            })
            
        case 4:
            controllerObject.openUrl(linkID: indexPath.row)
            
            
        case 6:
            controllerObject.openUrl(linkID: indexPath.row)
            
        default:
            break
        }
    }
    
    
    func setViewLayouts()
    {
        let topFrame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height * 0.05)
        sideMenuDrawerTopView.frame = topFrame
        imageVieww.frame = sideMenuDrawerTopView.frame
        
        let tableFrame = CGRect(x: 0, y: self.view.frame.size.height * 0.05, width: self.view.frame.size.width, height: self.view.frame.size.height * 0.95)
        sideMenuDrawerTable.frame = tableFrame
        
    }
    
    func audioSegmentConroller(cell: SideMenuDrawerTableCell)
    {
        switch cell.audioCellSegmentController.selectedSegmentIndex
        {
        case 0:
            print("single")
            controllerObject.setAudioPlayerFormat(option: 0)  // 0 for single verse
        case 1:
            print("cont...")
            controllerObject.setAudioPlayerFormat(option: 1)  // 1 for continuous
            
        case 2:
            print("repeat")
            controllerObject.setAudioPlayerFormat(option: 2)  // 2 for single repeat
            
        default:
            break
        }
    }
    
    func fontCellSegmentConroller(cell: SideMenuDrawerTableCell)
    {
        switch cell.fontCellSegmentController.selectedSegmentIndex
        {
        case 0:
            
            Colors.fontSize = 32
        case 1:
            
            Colors.fontSize = 40
        case 2:
            
            Colors.fontSize = 48
        default:
            break
        }
        
    }
    
}
