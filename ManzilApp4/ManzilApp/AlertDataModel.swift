//
//  AlertDataModel.swift
//  ManzilApp
//
//  Created by Admin on 7/26/17.
//  Copyright © 2017 Mujadidia. All rights reserved.
//

import Foundation


class AlertDataModel
{
    var alertDictionary : [String:String] =
        ["Share with a friend" : "Al-Manzil is a collection of Quranic verses that are to be recited as a means of protection and antidote. \nAl-Ruqya Al-Shariah protects from Black Magic, Jinn, Witchcraft, Sorcery, and the Evil Eye.  \n\nDownload this App from AppStore here:\nitunes.apple.com/pk/app/al-manzil-alruqyah-alshariah/id1276611544?mt=8"]
}
