//
//  AppShowCaseCollectionViewCell.swift
//  ManzilApp
//
//  Created by Admin on 8/3/17.
//  Copyright © 2017 Mujadidia. All rights reserved.
//

protocol AppShowcaseCellProtocol {
    func appShowCaseBackButon()
    func appStoreLinkButton()
}

import UIKit


class AppShowCaseCollectionViewCell: UICollectionViewCell {
    
    var delegate : AppShowcaseCellProtocol?
    
    @IBOutlet weak var appShowCaseImageView: UIImageView!
    @IBOutlet weak var appShowCaseBackButton: UIButton!
    @IBOutlet weak var appShowCaseTopLabel: UILabel!
    
    @IBOutlet weak var appStoreLinkButton: UIButton!
    
    @IBAction func appStoreLinkButton(_ sender: Any) {
        
        if let delegate = delegate
        {
            delegate.appStoreLinkButton()
        }
        
    }
    
    @IBAction func appShowCaseBackButton(_ sender: Any) {
        if let delegate = delegate
        {
            delegate.appShowCaseBackButon()
        }
    }
    
    
    
}
