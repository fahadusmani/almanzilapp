//
//  DetailViewModel.swift
//  ManzilApp
//
//  Created by Admin on 7/11/17.
//  Copyright © 2017 Mujadidia. All rights reserved.
//

import Foundation


class DetailViewModel
{
    
    
    var dataClassObject = MainApplicationData()
    var dataForDetailView : [String] = []
    
    func sendDataToController(arabicKey: Int, row: Int) -> [String]
    {
        var dataForDetailView : [String] = []
        
        switch row
        {
        case 0:
            dataForDetailView.append(dataClassObject.introArabic)
            dataForDetailView.append(dataClassObject.arabicData1[arabicKey])
            dataForDetailView.append(dataClassObject.introEnglish)
            dataForDetailView.append(dataClassObject.englishTranslation[arabicKey])
            break
            
        case 2:
            
            dataForDetailView.append(dataClassObject.introEnglish)
            dataForDetailView.append(dataClassObject.englishTranslation[arabicKey])
            break
            
        default:
            break
        }
        
        return dataForDetailView
    }
    
    
    
}
