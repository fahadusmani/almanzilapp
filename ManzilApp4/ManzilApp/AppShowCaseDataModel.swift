//
//  AppShowCaseDataModel.swift
//  ManzilApp
//
//  Created by Admin on 8/3/17.
//  Copyright © 2017 Mujadidia. All rights reserved.
//

import Foundation

struct AppShowCaseDataStruct
{
    var imageName = ""
    var appLink = ""
    var logoImage = ""
    var mobileImage = ""
    var descriptionText = ""
}

class AppShowCaseDataModel
{
    //var dataa = [AppShowCaseDataStruct]()
    var images : [String] = ["AppShowcase_SPro_mobile","AppShowcase_Dua O Azkar","AppShowcase_discover","4","5"]
    
    var logoImages : [String] = ["AppShowcase_SPro_mobile","AppShowcase_Dua_logo","AppShowcase_Discover_logo","4","5"]
    
    var mobileImages : [String] = ["AppShowcase_SPro_mobile","AppShowcase_Dua_mobile","AppShowcase_Discover_mobile","4","5"]
    
    var descriptionText : [String] = ["","","",""]
    
    var link : [String] = [
        "itms-apps://itunes.apple.com/pk/developer/mujadida-inc/id1240213107",
        "itms-apps://itunes.apple.com/pk/developer/mujadida-inc/id1240213107",
        "itms-apps://itunes.apple.com/pk/developer/mujadida-inc/id1240213107",
        "itms-apps://itunes.apple.com/pk/developer/mujadida-inc/id1240213107",
        "itms-apps://itunes.apple.com/pk/developer/mujadida-inc/id1240213107"
    ]
    
    
    func appShowCaseData(index: Int) -> [AppShowCaseDataStruct]
    {
        var dataa = [AppShowCaseDataStruct]()
        var data = AppShowCaseDataStruct()
        data.imageName = images[index]
        data.appLink = link[index]
        data.logoImage = logoImages[index]
        data.mobileImage = mobileImages[index]
        data.descriptionText = descriptionText[index]
        
        dataa = [data]
        return dataa
    }
    
}
