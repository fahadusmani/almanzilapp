//
//  AppShowCaseView.swift
//  ManzilApp
//
//  Created by Admin on 8/1/17.
//  Copyright © 2017 Mujadidia. All rights reserved.
//

import UIKit

class AppShowCaseView: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout, AppShowcaseCellProtocol {
    
    @IBOutlet weak var appShowCaseCollectionView: UICollectionView!
    
    @IBOutlet weak var myPageControl: UIPageControl!
    
    
    @IBOutlet weak var appShowCaseButton: UIButton!
    
    //  @IBOutlet weak var appShowCaseLabel: UILabel!
    
    var appShowCaseLabel = UILabel(frame: CGRect(x: UIScreen.main.bounds.width * 0.2, y: UIScreen.main.bounds.height * 0.015, width: UIScreen.main.bounds.width * 0.6, height: UIScreen.main.bounds.height * 0.1))
    
    
    var pageIndex = 0
    var cellClassObject = AppShowCaseCollectionViewCell()
    var controllerObject = AppShowCaseViewController()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        myPageControl.currentPageIndicatorTintColor = UIColor.black
            //Colors.themeColor
        myPageControl.pageIndicatorTintColor = UIColor.lightGray
        
      // setTopLabel()
        
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        var cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! AppShowCaseCollectionViewCell
        
        cell.delegate = self
        let imageInfo = controllerObject.getImageName(index: indexPath.row)
        cell.appShowCaseImageView.image = UIImage(named: imageInfo[0].imageName)
        // cell.appShowCaseTopLabel.text = imageInfo[0].descriptionText
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellWidth = UIScreen.main.bounds.size.width
        let cellHeight = appShowCaseCollectionView.bounds.size.height
        return CGSize(width: cellWidth, height: cellHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        //  self.myPageControl.currentPage = indexPath.row
        // self.myPageControl.currentPageIndicatorTintColor = Colors.themeColor
        pageIndex = indexPath.row
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        self.myPageControl.currentPage = pageIndex
    }
    
    
    func appShowCaseBackButon()
    {
        dismiss(animated: true, completion: nil)
    }
    
    
   /* func setTopLabel()
    {
        appShowCaseLabel.text = "View in App Store"
        appShowCaseLabel.textAlignment = .center
        appShowCaseLabel.textColor = Colors.cardViewBackgroundColor
            
        appShowCaseLabel.font = UIFont(name: "NotoSans", size: 18.0)
        self.view.addSubview(appShowCaseLabel)
    } */
    
    
    
     func appStoreLinkButton() {
        
        let data = controllerObject.getImageName(index: pageIndex)
        let urlString = data[0].appLink
        UIApplication.shared.openURL(URL(string:"\(urlString)")!)
        
    }
    
    
    
    
    
}
