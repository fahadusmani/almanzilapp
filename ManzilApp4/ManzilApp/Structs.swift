//
//  AppDataStructs.swift
//  ManzilApp
//
//  Created by Admin on 8/10/17.
//  Copyright © 2017 Mujadidia. All rights reserved.
//

import Foundation

struct AppDataStruct
{
    var reference : String?
    var arabic : String?
    var roman : String?
    var translation : String?
    var audio : String?
    var id : Int?
}
