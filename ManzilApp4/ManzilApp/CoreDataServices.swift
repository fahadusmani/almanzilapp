//
//  CoreDataServices.swift
//  ManzilApp
//
//  Created by Admin on 8/9/17.
//  Copyright © 2017 Mujadidia. All rights reserved.
//

import Foundation
import CoreData




class CoreDataServices
{
    
    func getAllData(from table : String) -> [NSManagedObject]
    {
        var data =  [NSManagedObject]()
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let managedContext =
            appDelegate.persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        
        let entityDescription = NSEntityDescription.entity(forEntityName: table, in: managedContext)
        
        fetchRequest.entity = entityDescription
        
        
        do {
            
            let results = try managedContext.fetch(fetchRequest)
            
            data = results as! [NSManagedObject]
            
        }
            
        catch {
        }
        
        return data
        
    }
    
    
    
    
    func save(in  table: String , items : [(arabic:String, roman:String, translation:String,audio :String, ref: String)]?)->Bool
    {
        
        var id = 0.0
        
        // if(table == "ManzilAppDB"){
        
        for item in  items!
        {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            
            let managedContext =
                appDelegate.persistentContainer.viewContext
            
            // let managedContext = appDelegate.managedObjectContext
            
            let entity : NSEntityDescription =  NSEntityDescription.entity(forEntityName: table,
                                                                           in:managedContext)!
            
            let entityData = NSManagedObject(entity: entity,
                                             insertInto: managedContext)
            
            entityData.setValue(item.arabic, forKeyPath: "arabic")
            entityData.setValue(item.roman, forKeyPath: "roman")
            entityData.setValue(item.translation, forKeyPath: "translation")
            entityData.setValue(item.audio, forKeyPath: "audio")
            entityData.setValue(item.ref, forKeyPath: "reference")
            entityData.setValue(id, forKeyPath: "id")
            
            do
            {
                try managedContext.save()
                appDelegate.saveContext()
                id = id + 1
            }
        
        catch
        {
            return false
        }
    }
    //}
    return true
}


func fetch(from table: String) -> [AppDataStruct]
{
    var bookData = [AppDataStruct]()
    let AppDel:AppDelegate = (UIApplication.shared.delegate as! AppDelegate)
    
    let context =
        AppDel.persistentContainer.viewContext
    
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: table)
    
    request.returnsObjectsAsFaults = false
    
    let results: NSArray = try! context.fetch(request) as NSArray
    
    
    
    if(results.count > 0)
    {
        var data = AppDataStruct()
        
        let x = (results.count) - 1
        
        for i in 0...x
        {
            let res = results[i] as! NSManagedObject
            let arabic = res.value(forKey: "arabic") as! String
            let roman = res.value(forKey: "roman") as! String
            let translation = res.value(forKey: "translation") as! String
            let audio = res.value(forKey: "audio") as! String
            let reference = res.value(forKey: "reference") as! String
            let id = res.value(forKey: "id") as! Int
            
            data.arabic = arabic
            data.roman = roman
            data.translation = translation
            data.audio = audio
            data.reference = reference
            data.id = id
            bookData.append(data)
        }
    }
    return bookData
}



func delete(from table: String) {
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let managedContext = appDelegate.persistentContainer.viewContext
    let DelAllReqVar = NSBatchDeleteRequest(fetchRequest: NSFetchRequest<NSFetchRequestResult>(entityName: table))
    do {
        try managedContext.execute(DelAllReqVar)
    }
    catch
    {
        print(error)
    }
    
}


}






