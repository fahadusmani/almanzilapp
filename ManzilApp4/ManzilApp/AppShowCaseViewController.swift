//
//  AppShowCaseViewController.swift
//  ManzilApp
//
//  Created by Admin on 8/3/17.
//  Copyright © 2017 Mujadidia. All rights reserved.
//

import Foundation

class AppShowCaseViewController
{
    var dataModelObject = AppShowCaseDataModel()
    
    
    func getImageName(index: Int) -> [AppShowCaseDataStruct]
    {
        let data = dataModelObject.appShowCaseData(index: index)
        return data
    }
}
