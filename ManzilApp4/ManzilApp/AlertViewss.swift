//
//  AlertViews.swift
//  ManzilApp
//
//  Created by Admin on 7/26/17.
//  Copyright © 2017 Mujadidia. All rights reserved.
//

import Foundation


class AlertViewss
{
    var dataModel = AlertDataModel()
    
    func showShareWidget(key: String) -> UIActivityViewController
    {
        let text = dataModel.alertDictionary[key]
        var shareWidget = UIActivityViewController(activityItems: [text], applicationActivities: nil)
        
        return shareWidget
    }
    
    func showBookAlert() -> UIAlertController
    {
        var alert = UIAlertController(title: "Last read location", message: "Do u wish to continue where you left off", preferredStyle: UIAlertControllerStyle.alert)
          
        return alert
    }
    
}
