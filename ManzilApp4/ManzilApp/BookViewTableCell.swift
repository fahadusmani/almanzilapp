//
//  BookViewTableCell.swift
//  ManzilApp
//
//  Created by Admin on 7/11/17.
//  Copyright © 2017 Mujadidia. All rights reserved.
//

protocol BookViewCellProtocol
{
}

import UIKit

class BookViewTableCell: UITableViewCell
{
    var delegate : BookViewCellProtocol?
    
    
    
    
    @IBOutlet weak var newLabel: UILabel!
    @IBOutlet weak var backgroundCardView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func layoutSubviews() {
        let font = UIFont(name: "noorehuda", size: CGFloat(Colors.fontSize))
        newLabel.font = font
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
}
