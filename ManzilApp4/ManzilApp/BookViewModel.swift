//
//  BookViewModel.swift
//  ManzilApp
//
//  Created by Admin on 7/11/17.
//  Copyright © 2017 Mujadidia. All rights reserved.
//

import Foundation


class BookViewModel
{
    var dataClassObject = MainApplicationData()
    var dataForBookView = ""
    var audioList : [String] = []
    
    
    func getAudioData() -> [AppDataStruct]
    {
        var data : [AppDataStruct] = []
        if(MainVc.bookNumber == 1)
        {
            data = SharedResources.fetchDataFromDB(from: "ManzilAppDB")
        }
        else
        {
            data = SharedResources.fetchDataFromDB(from: "ALRuqyaDB")
        }
        return data
    }
    
    
      func getBookData() -> [AppDataStruct]
    {
        if(MainVc.bookNumber == 1)
        {
            return dataClassObject.fetchData(from: "ManzilAppDB")
        }
            
        else
        {
            return dataClassObject.fetchData(from: "ALRuqyaDB")
        }
        
    }
    
}
