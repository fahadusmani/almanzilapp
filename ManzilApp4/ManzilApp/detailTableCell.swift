//
//  detailTableCell.swift
//  ManzilApp
//
//  Created by Admin on 7/11/17.
//  Copyright © 2017 Mujadidia. All rights reserved.
//

import UIKit

protocol DetailViewCellProtocol
{
    
}

class detailTableCell: UITableViewCell {
    
    var delegate : DetailViewCellProtocol?
    
    @IBOutlet weak var customLabel: UILabel!
    @IBOutlet weak var customTitleLabel: UILabel!
 
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    
}
