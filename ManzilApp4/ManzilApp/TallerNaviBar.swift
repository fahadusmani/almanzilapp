//
//  TallerNaviBar.swift
//  ManzilApp
//
//  Created by Admin on 8/24/17.
//  Copyright © 2017 Mujadidia. All rights reserved.
//

import UIKit

class TallerNaviBar: UINavigationBar
{
    override func sizeThatFits(_ size: CGSize) -> CGSize
    {
        let newSize :CGSize = CGSize(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height * 0.11)
        return newSize
    }
    
}
