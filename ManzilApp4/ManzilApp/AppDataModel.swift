//
//  CoreDataModel.swift
//  ManzilApp
//
//  Created by Admin on 7/19/17.
//  Copyright © 2017 Mujadidia. All rights reserved.
//

import Foundation

class AppDataModel
{
    let defaults = UserDefaults.standard
    
    
    func saveApplicationDefaults(appStartup: Int, bookNumber: Int, audioNumber1: Int, audioNumber2: Int, audioFormat: Int, fontSize: Int)
    {
        defaults.set(appStartup, forKey: "appStartup")
        defaults.set(bookNumber, forKey: "bookNumber")
        defaults.set(audioNumber1, forKey: "audioNumber1")
        defaults.set(audioNumber2, forKey: "audioNumber2")
        defaults.set(audioFormat, forKey: "audioFormat")
        defaults.set(fontSize, forKey: "fontSize")
    }
    
    func fetchApplicationDefaults()
    {
        if let appStartup = defaults.value(forKey: "appStartup") as! Int?
        {
            SharedResources.appStartup = appStartup
        }
        else
        {
            SharedResources.appStartup = 0
        }
        
        if let bookNumber = defaults.value(forKey: "bookNumber") as! Int?
        {
            MainVc.bookNumber = bookNumber
        }
        else
        {
            MainVc.bookNumber = 1
        }
        
        if let audioNumber1 = defaults.value(forKey: "audioNumber1") as! Int?
        {
            MainVc.book1Location = audioNumber1
        }
        else
        {
            MainVc.book1Location = 0
        }
        
        if let audioNumber2 = defaults.value(forKey: "audioNumber2") as! Int?
        {
            MainVc.book2Location = audioNumber2
        }
        else
        {
            MainVc.book2Location = 0
        }
        
        if let audioFormat = defaults.value(forKey: "audioFormat") as! Int?
        {
            AudioPlayer.audioPlayerFormat = audioFormat
        }
        else
        {
            AudioPlayer.audioPlayerFormat = 0
        }
        
        if let fontSize = defaults.value(forKey: "fontSize") as! Int?
        {
            Colors.fontSize = Double(fontSize)
        }
        else
        {
            Colors.fontSize = 32
        }
        
        
    }
    
    
    func saveBookmark(audioNumber: Int)
    {
        if(MainVc.bookNumber == 1)
        {
            defaults.set(audioNumber, forKey: "audioNumberbook1")
        }
        else
        {
            defaults.set(audioNumber, forKey: "audioNumberbook2")
        }
    }
    
    
}
