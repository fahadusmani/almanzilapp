//
//  SplashView.swift
//  ManzilApp
//
//  Created by Admin on 8/29/17.
//  Copyright © 2017 Mujadidia. All rights reserved.
//

import UIKit
import BubbleTransition


class SplashView: UIViewController, UIViewControllerTransitioningDelegate {
    
    @IBOutlet weak var logoImageLabel: UIImageView!
    
    let transition = BubbleTransition()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.update), userInfo: nil, repeats: false);
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    public override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let controller = segue.destination
        controller.transitioningDelegate = self
        controller.modalPresentationStyle = .custom
        
    }
    
    
    public func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        transition.duration = 0.8
        transition.transitionMode = .present
        transition.startingPoint = self.logoImageLabel.center
        transition.bubbleColor = UIColor.white
        return transition
    }
    
    public func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        transition.transitionMode = .dismiss
        transition.startingPoint = self.logoImageLabel.center
        transition.bubbleColor = UIColor.blue
        
        return transition
    }

    
    func update() {
        
        performSegue(withIdentifier: "toHomeScreen", sender: self)
    }
    
    
    
}
