//
//  Colors.swift
//  ManzilApp
//
//  Created by Admin on 7/24/17.
//  Copyright © 2017 Mujadidia. All rights reserved.
//

import Foundation

class Colors
{
    static var fontSize = 32.0
    
    static var themeColor = UIColor(red: CGFloat(19)/255.0, green: CGFloat(63)/255.0, blue: CGFloat(138)/255.0, alpha: 1)
    
    static var cellBackGroundColor = UIColor(red: CGFloat(243)/255.0, green: CGFloat(243)/255.0, blue: CGFloat(243)/255.0, alpha: 1)
    
    static var detailTitleBackGroundColor = UIColor(red: CGFloat(226)/255.0, green: CGFloat(226)/255.0, blue: CGFloat(226)/255.0, alpha: 1)
    
    static var cellTextColor = UIColor(red: CGFloat(0)/255.0, green: CGFloat(0)/255.0, blue: CGFloat(0)/255.0, alpha: 1)
    
    static var ButtonsTextColor = UIColor(red: CGFloat(255)/255.0, green: CGFloat(255)/255.0, blue: CGFloat(240)/255.0, alpha: 1)
    
    static var cardViewBackgroundColor =  UIColor(red: CGFloat(250)/255.0, green: CGFloat(250)/255.0, blue: CGFloat(250)/255.0, alpha: 1)
    
     static var mainViewBackgroundColor =  UIColor(red: CGFloat(243)/255.0, green: CGFloat(243)/255.0, blue: CGFloat(243)/255.0, alpha: 1)
    //UIColor(red: CGFloat(19)/255.0, green: CGFloat(63)/255.0, blue: CGFloat(138)/255.0, alpha: 1)
    
    static var cellSelectionColor = UIColor.lightGray
    
    
}
