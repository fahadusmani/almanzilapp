//
//  UrlServices.swift
//  ManzilApp
//
//  Created by Admin on 7/25/17.
//  Copyright © 2017 Mujadidia. All rights reserved.
//

import Foundation

class UrlServices
{
    var dataModelObject = UrlServicesModel()
    
    func openUrl(linkID: Int)
    {
        var urlString = dataModelObject.urlDictionary[linkID]
    UIApplication.shared.openURL(URL(string:"\(urlString!)")!)
    }
}
