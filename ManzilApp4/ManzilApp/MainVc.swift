//
//  MainVc.swift
//  ManzilApp
//
//  Created by Admin on 8/21/17.
//  Copyright © 2017 Mujadidia. All rights reserved.
//

import UIKit
import KYDrawerController


// First Screen ViewController
class MainVc: UIViewController, UITableViewDelegate, UITableViewDataSource, BookViewCellProtocol,KYDrawerControllerDelegate, bookViewControllerDelegate {
    // class outlets & var declerations
    
    @IBOutlet weak var mainSegmentedConrol: UISegmentedControl!
    @IBOutlet weak var buttonsView: UIView!
    @IBOutlet weak var previousButton: UIButton!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var OpenSideMenuButton: UIBarButtonItem!
    @IBOutlet weak var myTable: UITableView!
    @IBOutlet weak var navBarBackgroundView: UIView!
    @IBOutlet weak var myNavItem: UINavigationItem!
    @IBOutlet weak var navBarBackButton: UIBarButtonItem!
    
    static var doNotScroll = false
    static var bookNumber = 1
    static var refresh = 0
    static var book1Location = 0
    static var book2Location = 0
    static var firstLaunch = true
    var indexForDetailView = 0
    var isPlayerPlaying = false
    var heightAtIndexPath = NSMutableDictionary()
    var alertViewObject = AlertViewss()
    var controllerObject = BookViewController()
    var mainVcFunctions = MainVcFunctions()
    var data : [AppDataStruct] = []
    var str : [String] = []
    
    
    // Segment Controller Actions
    @IBAction func mainSegmentedConrol(_ sender: Any) {
        if(isPlayerPlaying)
        {
            //if audio is already playing then stop audio and update playerIcon
            controllerObject.stopAudioPlayer()
            updatePlayer()
        }
        switch mainSegmentedConrol.selectedSegmentIndex
        {
        case 0:
            //if first segment(Al-Manzil) is selected than perform this
            MainVc.book2Location = AudioPlayer.audioNumber
            MainVc.bookNumber = 1
            AudioPlayer.audioNumber = MainVc.book1Location
            
        case 1:
            //if first segment(Al-Ruqyah AlShariah) is selected than perform this
            MainVc.book1Location = AudioPlayer.audioNumber
            MainVc.bookNumber = 2
            AudioPlayer.audioNumber = MainVc.book2Location
            
        default:
            break
        }
        
        MainVc.refresh = 1
        MainVc.doNotScroll = true
        data = []
        str = []
        data = getData()
        str = getAttributedData(data: data)
        
        self.myTable.reloadData()
        
        var indexPath = NSIndexPath(row: 0, section: 0)
        self.myTable.scrollToRow(at: indexPath as IndexPath, at: UITableViewScrollPosition.top, animated: false)
    }
    
    
    @IBAction func previousButton(_ sender: Any) {
        if(isPlayerPlaying)
        {
            controllerObject.setAudioPlayer(isPlayerPlaying: isPlayerPlaying, Jump: 1)
        }
        else
        {
            controllerObject.moveBackward()
        }
        
    }
    @IBAction func playButton(_ sender: Any) {
        if(isPlayerPlaying)
        {
            controllerObject.setAudioPlayer(isPlayerPlaying: isPlayerPlaying, Jump: 0)
            isPlayerPlaying = false
            playButton.setImage(#imageLiteral(resourceName: "play-button"), for: .normal)
        }
        else
        {
            controllerObject.setAudioPlayer(isPlayerPlaying: isPlayerPlaying, Jump: 0)
            isPlayerPlaying = true
            playButton.setImage(UIImage(named: "pause.png"), for: .normal)
        }
        
    }
    
    
    @IBAction func nextButton(_ sender: Any) {
        if(isPlayerPlaying)
        {
            
            //AudioPlayer.audioNumber = AudioPlayer.audioNumber + 1
            controllerObject.setAudioPlayer(isPlayerPlaying: isPlayerPlaying, Jump: 2)
        }
        else
        {
            controllerObject.moveForward()
        }
        
    }
    
    
    @IBAction func OpenSideMenuButton(_ sender: Any) {
        if let drawerController = navigationController?.parent as? KYDrawerController
        {
            drawerController.drawerWidth = self.view.frame.width * 0.8
            drawerController.containerViewMaxAlpha = 0.3
            drawerController.drawerDirection = .left
            drawerController.setDrawerState(.opened, animated: true)
            
        }
    }
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        myTable.estimatedRowHeight = 50
        myTable.rowHeight = UITableViewAutomaticDimension
        controllerObject.delegate = self
        
        
        data = getData()
        str = []
        str = getAttributedData(data: data)
        
        mainVcFunctions.setNavBar(mainView: self.view, navBar: self.navigationController!, background: self.navBarBackgroundView, sideButtonItem:  self.OpenSideMenuButton)
        
        mainVcFunctions.setView(view: self.view, table: myTable, segment: self.mainSegmentedConrol)
        
        mainVcFunctions.setPlayerView(mainView: self.view, view: self.buttonsView, button1: self.previousButton, button3: self.nextButton, button2: self.playButton)
        
        
        if let drawerController = navigationController?.parent as? KYDrawerController
        {
             drawerController.screenEdgePanGestureEnabled = true
            drawerController.drawerWidth = self.view.frame.size.width * 0.8
            drawerController.drawerDirection = .left
            drawerController.containerViewMaxAlpha = 0.3
        }
        
        
        
        let font = UIFont(name: "NotoSans", size: self.mainSegmentedConrol.frame.width * 0.045)
        mainSegmentedConrol.setTitleTextAttributes([NSFontAttributeName: font], for: .normal)
        
        self.navigationController?.navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont(name: "NotoSans-Bold", size: UIScreen.main.bounds.width * 0.048),                                                                                                    NSForegroundColorAttributeName : Colors.mainViewBackgroundColor
            
        ]
        
        OpenSideMenuButton.setBackgroundVerticalPositionAdjustment(-9, for: UIBarMetrics.default)
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        
        if(MainVc.firstLaunch)
        {
            if(MainVc.bookNumber == 1)
            {
                AudioPlayer.audioNumber = MainVc.book1Location
                mainSegmentedConrol.selectedSegmentIndex = 0
            }
            else
            {
                AudioPlayer.audioNumber = MainVc.book2Location
                mainSegmentedConrol.selectedSegmentIndex = 1
            }
            if(AudioPlayer.audioNumber > 0)
            {
                showBookmarkAlert()
            }
            MainVc.firstLaunch = false
        }
        self.myTable.reloadData()
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        self.myTable.reloadData()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return str.count-1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as! BookViewTableCell
        
        cell.delegate = self
        cell.newLabel.text = str[indexPath.row + 1]
        cell = mainVcFunctions.setCellView(cell: cell)
        cell.newLabel.textColor = Colors.cellTextColor
        if(indexPath.row == AudioPlayer.audioNumber)
        {
            cell.newLabel.textColor = Colors.themeColor
        }
        cell.setNeedsUpdateConstraints()
        cell.updateConstraintsIfNeeded()
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        indexForDetailView = indexPath.row + 1
        performSegue(withIdentifier: "toDetailView", sender: self)
    }
    
    
    func getData() -> [AppDataStruct]
    {
        let data = controllerObject.getData()
        return data
    }
    
    func getAttributedData(data: [AppDataStruct]) -> [String]
    {
        var newData : [String] = []
        if(data.count > 1)
        {
            for i in 0...data.count-1
            {
                //let strr = mainVcFunctions.getAttributedString(stringValue: data[i].arabic!)
                let strr = data[i].arabic!
                newData.append(strr)
            }
        }
        else
        {
            newData = []
        }
        return newData
    }
    
    
    func moveTable(doScroll: Bool)
    {
        if(doScroll)
        {
            print(AudioPlayer.audioNumber)
            myTable.reloadData()
            var indexPath = NSIndexPath(row: AudioPlayer.audioNumber, section: 0)
            self.myTable.selectRow(at: indexPath as IndexPath, animated: false, scrollPosition: UITableViewScrollPosition.top)
        }
            
        else
        {
            
        }
    }
    
    
    func setPlayerIcon()
    {
        playButton.setImage(#imageLiteral(resourceName: "play-button"), for: .normal)
    }
    
    func updatePlayer()
    {
        isPlayerPlaying = false
        playButton.setImage(#imageLiteral(resourceName: "play-button"), for: .normal)
    }
    
    
    func showBookmarkAlert()
    {
        let alert = alertViewObject.showBookAlert()
        
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action: UIAlertAction!) in
            
            var indexPath = NSIndexPath(row: AudioPlayer.audioNumber, section: 0)
            self.myTable.scrollToRow(at: indexPath as IndexPath, at: UITableViewScrollPosition.middle, animated: false)
        }))
        
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: { (action: UIAlertAction!) in
            AudioPlayer.audioNumber = 0
            var indexPath = NSIndexPath(row: AudioPlayer.audioNumber, section: 0)
            self.myTable.scrollToRow(at: indexPath as IndexPath, at: UITableViewScrollPosition.middle, animated: false)
            MainVc.book1Location = 0
            MainVc.book2Location = 0
            
            self.myTable.reloadData()
        }))
        
        present(alert, animated: true, completion: nil)
    }
    
    
    
    func openDetailView(detailData: [AppDataStruct], index: Int)
    {
    }
    
    override public func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        controllerObject.stopAudioPlayer()
        if(segue.identifier == "toDetailView")
        {
            controllerObject.stopAudioPlayer()
            updatePlayer()
            
            var dest = segue.destination as! DetailView
            dest._detailData = data
            dest._index = indexForDetailView
        }
    }
    
    
    
}






