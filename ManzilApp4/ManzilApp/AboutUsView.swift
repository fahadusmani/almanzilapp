//
//  AboutUsView.swift
//  ManzilApp
//
//  Created by Admin on 8/1/17.
//  Copyright © 2017 Mujadidia. All rights reserved.
//

import UIKit

class AboutUsView: UIViewController
{
    //about us view
    @IBOutlet weak var topImageView: UIImageView!
    @IBOutlet weak var detailTextLabel: UILabel!
    @IBOutlet weak var facebookButton: UIButton!
    @IBOutlet weak var twitterButton: UIButton!
    @IBOutlet weak var gmailButton: UIButton!
    @IBOutlet weak var backBarButton: UIBarButtonItem!
    @IBOutlet weak var aboutUsNavBar: UINavigationBar!
    var controllerObject = SideMenuViewController()
    
    @IBAction func facebookButton(_ sender: Any)
    {
        controllerObject.openUrl(linkID: 7)
    }
    
    @IBAction func twitterButton(_ sender: Any)
    {
        controllerObject.openUrl(linkID: 8)
    }
    
    @IBAction func gmailButton(_ sender: Any)
    {
        controllerObject.openUrl(linkID: 9)
    }
    
    @IBAction func backBarButton(_ sender: UIBarButtonItem)
    {
        dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        aboutUsNavBar.backgroundColor = Colors.themeColor.withAlphaComponent(0.9)
        aboutUsNavBar.setBackgroundImage(#imageLiteral(resourceName: "navBarBackground"), for: UIBarMetrics.default)
        aboutUsNavBar.barTintColor = Colors.mainViewBackgroundColor
        
        aboutUsNavBar.titleTextAttributes = [
            NSFontAttributeName: UIFont(name: "NotoSans-Bold", size: UIScreen.main.bounds.width * 0.053),                                                                                                    NSForegroundColorAttributeName : Colors.mainViewBackgroundColor
        ]
        
        let font = UIFont(name: "NotoSans", size: 18.0)
        detailTextLabel.font = font
        
        backBarButton.tintColor = Colors.mainViewBackgroundColor
        
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    
}
