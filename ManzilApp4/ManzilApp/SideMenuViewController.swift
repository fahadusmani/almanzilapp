//
//  SideMenuViewController.swift
//  ManzilApp
//
//  Created by Admin on 7/19/17.
//  Copyright © 2017 Mujadidia. All rights reserved.
//

import Foundation

protocol SideProtocol {
    //func update()
}

class SideMenuViewController
{
    var audioClassObject = AudioPlayer()
    //var newObj = BookViewController()
    var delegate : SideProtocol?
    var urlClassObject = UrlServices()
    var alertClassObject = AlertViewss()
    
    func setAudioPlayerFormat(option: Int)
    {
        AudioPlayer.audioPlayerFormat = option
    }
    
    
    func openUrl(linkID: Int)
    {
        urlClassObject.openUrl(linkID: linkID)
    }
    
    func showShareWidget(key: String) -> UIActivityViewController
    {
        let shareWidget = alertClassObject.showShareWidget(key: key)
        return shareWidget
    }
    
}
