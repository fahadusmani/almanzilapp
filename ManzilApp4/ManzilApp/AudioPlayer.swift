 //
 //  AudioPlayer.swift
 //  ManzilApp
 //
 //  Created by Admin on 7/17/17.
 //  Copyright © 2017 Mujadidia. All rights reserved.
 //
 protocol AudioClassProtocol
 {
    func scrollTableView()
    func updatePlayerIcon()
 }
 
 
 import Foundation
 import AVFoundation
 import MediaPlayer
 
 
 class AudioPlayer : AVAudioPlayer
 {
    var myDelegate: AudioClassProtocol!
    
    static var audioPlayerFormat = 0 //0 = playing single verse, 1 = continuous, 2 =repeat
    
    var favoriteAudio = 0
    static var audioData : [AppDataStruct] = []
    var audioList : [AppDataStruct] = []
    var audioList2 : [String] = []
    static var audioNumber = 0
    static var audioNumber2 = 0
    var scroll = 1
    var audioItems: [AVPlayerItem] = []
    var player = AVPlayer()
    var audioDurationSeconds = 0.0
    
    func updateAudioPlayer(isPlayerPlaying: Bool, data: [AppDataStruct], jump: Int)
    {
        AudioPlayer.audioData = data
        if(isPlayerPlaying)
        {
            if(jump == 1)
            {
                if(AudioPlayer.audioNumber > 0)
                {
                    AudioPlayer.audioNumber2 = AudioPlayer.audioNumber + 1
                    if(AudioPlayer.audioNumber > data.count - 1)
                    {
                        AudioPlayer.audioNumber = data.count - 1
                    }
                    
                    AudioPlayer.audioNumber = AudioPlayer.audioNumber - 1
                    AudioPlayer.audioNumber2 = AudioPlayer.audioNumber2 - 1
                    stopPlayer()
                    playMusic(audios: data, audioNumber: AudioPlayer.audioNumber2)
                }
                else
                {
                    AudioPlayer.audioNumber = 0
                    AudioPlayer.audioNumber2 = 1//audioList.count - 1
                    stopPlayer()
                    playMusic(audios: data, audioNumber: AudioPlayer.audioNumber2)
                }
                
            }
                
            else if(jump == 2)
            {
                if(AudioPlayer.audioNumber < data.count-1)
                {
                    AudioPlayer.audioNumber2 = AudioPlayer.audioNumber + 1
                    AudioPlayer.audioNumber2 = AudioPlayer.audioNumber2 + 1
                    AudioPlayer.audioNumber = AudioPlayer.audioNumber + 1
                    stopPlayer()
                    playMusic(audios: data, audioNumber: AudioPlayer.audioNumber2)
                }
                    
                else
                {
                    AudioPlayer.audioNumber = 0
                    myDelegate.updatePlayerIcon()
                    stopPlayer()
                }
            }
            else
            {
                stopPlayer()
            }
        }
        else
        {
            AudioPlayer.audioNumber2 = AudioPlayer.audioNumber + 1
            
            
            playMusic(audios: data, audioNumber: AudioPlayer.audioNumber2)
            
        }
        //setLockScreenButtons(audios: data)
        //updateLockScreenUI(data: AudioPlayer.audioData)
    }
    
    
    func playMusic(audios: [AppDataStruct], audioNumber: Int)
    {
        // setupLockScreenMediaPlayer(data: audios)
        // playPauseByMPPlayer()
        
        
        if(AudioPlayer.audioNumber2 < audios.count)
        {
            do
            {
                if let audioName = audios[audioNumber].audio! as String?
                {
                    
                    
                    let audio = audioName.replacingOccurrences(of: ".mp3", with: "")
                    let audioFileName = "audios/"+(audio)
                    
                    let url2 = Bundle.main.url(forResource: audioFileName, withExtension: "mp3")! as URL?
                    
                    audioList = audios
                    play(url1: url2! as NSURL)
                }
                
            }
                
            catch
            {
                print("Error")
            }
        }
        else
        {
            //setLockScreenButtons(audios: AudioPlayer.audioData)
            stopPlayer()
        }
    }
    
    func stopPlayer()   //stop audio
    {
        player.pause()
    }
    
    
    func play(url1 : NSURL)
    {
        
        if(AudioPlayer.audioNumber2 < audioList.count)
        {
            
            let asset = AVURLAsset(url: url1 as URL)
            let audioDuration = asset.duration
            audioDurationSeconds = CMTimeGetSeconds(audioDuration)
            updateLockScreenUI(data: AudioPlayer.audioData)
            
            let item = AVPlayerItem(url: url1 as URL)
            NotificationCenter.default.addObserver(self,selector:Selector("playerDidFinishPlaying"), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: item)
            
            player = AVPlayer(playerItem: item)
            
            //setBackgroundMusic()
            //setRouteChange()
            player.play()
            
            if(scroll == 1)
            {
                myDelegate?.scrollTableView()
            }
            scroll = 1
            
        }
    }
    
    func playerDidFinishPlaying()
    {
        if(AudioPlayer.audioPlayerFormat == 0)
        {
            myDelegate.updatePlayerIcon()
            stopPlayer()
        }
        else if(AudioPlayer.audioPlayerFormat == 2)
        {
            
            var audioName = audioList[AudioPlayer.audioNumber2].audio!
            audioName = audioName.replacingOccurrences(of: ".mp3", with: "")
            
            let audioFileName = "audios/"+(audioName)
            let url2 = Bundle.main.url(forResource: audioFileName, withExtension: "mp3")!
            
            scroll = 0
            
            play(url1: url2 as NSURL)
        }
            
        else
        {
            if(AudioPlayer.audioNumber2 < audioList.count-2)
            {
                var audioName = audioList[AudioPlayer.audioNumber2+1].audio!
                audioName = audioName.replacingOccurrences(of: ".mp3", with: "")
                
                let audioFileName = "audios/"+(audioName)
                let url2 = Bundle.main.url(forResource: audioFileName, withExtension: "mp3")!
                
                AudioPlayer.audioNumber2 = AudioPlayer.audioNumber2 + 1
                AudioPlayer.audioNumber = AudioPlayer.audioNumber + 1
                play(url1: url2 as NSURL)
                
            }
            else
            {
                myDelegate.updatePlayerIcon()
                AudioPlayer.audioNumber = 0
                stopPlayer()
            }
        }
        
        
    }
    
    func moveForward() -> Bool
    {
        if(AudioPlayer.audioNumber < audioList.count-2)
        {
            AudioPlayer.audioNumber = AudioPlayer.audioNumber + 1
            return true
        }
        else
        {
            return false
        }
    }
    
    func moveBackward() -> Bool
    {
        print("moved")
        if(AudioPlayer.audioNumber > 0)
        {
            AudioPlayer.audioNumber = AudioPlayer.audioNumber - 1
            print("b = \(AudioPlayer.audioNumber)")
            return true
        }
        else
        {
            return false
        }
    }
    
    
    func update()
    {
        print(player.volume)
    }
    
    func setBackgroundMusic()
    {
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback, with: [])
            print("AVAudioSession Category Playback OK")
            do {
                try AVAudioSession.sharedInstance().setActive(true)
                print("AVAudioSession is Active")
                
            } catch {
                print(error)
            }
        } catch {
            print(error)
        }
    }
    
    
    func setRouteChange()
    {
        
        let audioSession = AVAudioSession.sharedInstance()
        _ = try? audioSession.setCategory(AVAudioSessionCategoryPlayback, with: [])
        _ = try? audioSession.setActive(true)
        NotificationCenter.default.addObserver(self, selector: #selector(audioRouteChanged), name: .AVAudioSessionRouteChange, object: nil)
    }
    
    func audioRouteChanged(note: Notification) {
        if let userInfo = note.userInfo {
            if let reason = userInfo[AVAudioSessionRouteChangeReasonKey] as? Int {
                if reason == AVAudioSessionRouteChangeReason.oldDeviceUnavailable.hashValue {
                    // headphones plugged out
                    player.play()
                }
            }
        }
    }
    
    
    func setLockScreenButtons(audios: [AppDataStruct])
    {
        UIApplication.shared.beginReceivingRemoteControlEvents()
        
        let commandCenter = MPRemoteCommandCenter.shared()
        commandCenter.pauseCommand.addTarget { (event) -> MPRemoteCommandHandlerStatus in
            //Update your button here for the pause command
            self.stopPlayer()
            self.myDelegate.updatePlayerIcon()
            return .success
        }
        
        commandCenter.playCommand.addTarget { (event) -> MPRemoteCommandHandlerStatus in
            //Update your button here for the play command
            self.playMusic(audios: audios, audioNumber: AudioPlayer.audioNumber2)
            return .success
        }
        
        commandCenter.nextTrackCommand.addTarget { (event) -> MPRemoteCommandHandlerStatus in
            //Update your button here for the nextAudio command
            self.stopPlayer()
            self.updateAudioPlayer(isPlayerPlaying: true, data: audios, jump: 2)
            return .success
        }
        
        commandCenter.previousTrackCommand.addTarget { (event) -> MPRemoteCommandHandlerStatus in
            //Update your button here for the previousAudio command
            self.stopPlayer()
            self.updateAudioPlayer(isPlayerPlaying: true, data: audios, jump: 1)
            return .success
        }
        
    }
    
    
    func updateLockScreenUI(data: [AppDataStruct])
    {
        if(AudioPlayer.audioNumber2 < data.count-1)
        {
            let mpic = MPNowPlayingInfoCenter.default()
            var albumArtWork = MPMediaItemArtwork(image: #imageLiteral(resourceName: "lockScreenAppIcon"))
            
            mpic.nowPlayingInfo = [
                MPMediaItemPropertyTitle: data[AudioPlayer.audioNumber2].arabic!,
                MPMediaItemPropertyArtist:"Al-Manzil | Al-Ruqyah Al-Shariah",
                MPMediaItemPropertyArtwork: albumArtWork,
                MPMediaItemPropertyPlaybackDuration: audioDurationSeconds
                //MPNowPlayingInfoPropertyElapsedPlaybackTime: "audioPlayer.progress"
            ]
        }
    }
    
    
    
    
    
 }
 
