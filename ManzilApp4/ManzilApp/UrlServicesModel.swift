//
//  UrlServicesModel.swift
//  ManzilApp
//
//  Created by Admin on 7/25/17.
//  Copyright © 2017 Mujadidia. All rights reserved.
//

import Foundation

class UrlServicesModel
{
    
    var urlDictionary : [Int:String] = [
        4 : "itms-apps://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=1276611544",
        
        6 : "itms-apps://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=1245055646",
        9 : "http://www.mujadidia.com/",
        8 : "https://twitter.com/MujadidiaInc",
        7 : "https://www.facebook.com/MujadidiaInc/"
    ]
}
