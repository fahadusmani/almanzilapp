//
//  SharedResources.swift
//  ManzilApp
//
//  Created by Admin on 8/9/17.
//  Copyright © 2017 Mujadidia. All rights reserved.
//

import Foundation

class SharedResources
{
    static var coreDataObject = CoreDataServices()
    static var applicationDefaultsModel = AppDataModel()
    static var appStartup = 0
    
    static func saveCsvOnStart(csv : String, inTable: String)
    {
        
        if let contentsOfURL = Bundle.main.path(forResource: csv , ofType: "csv")
        {
            var error:NSError?
            if let items = csvParser(contentsOfURL, encoding: String.Encoding.utf8, error: &error)
            {
                //dataManger?.saveData(in: table , items: items)
                let dataa = items
                
                coreDataObject.save(in: inTable, items: items)
                
            }
        }
    }
    
    static func csvParser(_ contentsOfFile: String, encoding: String.Encoding, error: NSErrorPointer) -> [(arabic:String, roman:String, translation:String,audio :String, ref: String)]?
    {
        // Load the CSV file and parse it
        let delimiter = ","
        var items:[(arabic:String, roman:String, translation:String,audio :String, ref: String)]?
        do{
            let content = try String(contentsOfFile: contentsOfFile)
            items = []
            let lines:[String] = content.components(separatedBy: CharacterSet.newlines) as [String]
            
            
            for line in lines {
                var values:[String] = []
                if line != "" {
                    // For a line with double quotes
                    // we use NSScanner to perform the parsing
                    if line.range(of: "\"") != nil {
                        var textToScan:String = line
                        var value:NSString?
                        var textScanner:Scanner = Scanner(string: textToScan)
                        while textScanner.string != "" {
                            
                            if (textScanner.string as NSString).substring(to: 1) == "\"" {
                                textScanner.scanLocation += 1
                                textScanner.scanUpTo("\"", into: &value)
                                textScanner.scanLocation += 1
                            }
                            else {
                                textScanner.scanUpTo(delimiter, into: &value)
                            }
                            
                            
                            // Store the value into the values array
                            values.append(value as! String)
                            
                            // Retrieve the unscanned remainder of the string
                            if textScanner.scanLocation < textScanner.string.characters.count {
                                textToScan = (textScanner.string as NSString).substring(from: textScanner.scanLocation + 1)
                            } else {
                                textToScan = ""
                            }
                            textScanner = Scanner(string: textToScan)
                        }
                        
                        // For a line without double quotes, we can simply separate the string
                        // by using the delimiter (e.g. comma)
                    } else  {
                        values = line.components(separatedBy: delimiter)
                    }
                    
                    // Put the values into the tuple and add it to the items array
                    
                    var ref = ""
                    
        
                    if (values.count > 4)
                    {
                        if(values[4] != nil)
                        {
                            ref = values[4]
                        }
                    }
                    
                    let item = (arabic:values[0],roman:values[1],translation:values[2],audio:values[3],ref:ref)
                    items?.append(item)
                    
                }
            }
            return items
        }
        catch
        {
            print ("File Read Error")
            return nil
        }
    }
    
    
    static func fetchDataFromDB(from table: String) -> [AppDataStruct]
    {
        let data = SharedResources.coreDataObject.fetch(from: table)
        return data
    }
    
    static func deleteData(from table: String)
    {
        SharedResources.coreDataObject.delete(from: table)
    }
    
    
    static func fetchApplicationDefaults()
    {
        SharedResources.applicationDefaultsModel.fetchApplicationDefaults()
    }
    
    
    static func saveApplicationDefaults(appStartUp: Int, bookNumber: Int, audioNumber1: Int, audioNumber2: Int, audioFormat: Int, fontSize: Int)
    {
        SharedResources.applicationDefaultsModel.saveApplicationDefaults(appStartup: appStartUp, bookNumber: bookNumber, audioNumber1: audioNumber1, audioNumber2: audioNumber2, audioFormat: audioFormat, fontSize: fontSize)
    }
    
    
    
}
