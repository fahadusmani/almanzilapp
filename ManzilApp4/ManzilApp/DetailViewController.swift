//
//  DetailViewController.swift
//  ManzilApp
//
//  Created by Admin on 7/11/17.
//  Copyright © 2017 Mujadidia. All rights reserved.
//

protocol detailViewControllerProtocol
{
    func moveForward(listCount: Int)
    func moveBackward()
    func updatePlayer()
}

import Foundation

class DetailViewController : AudioClassProtocol
{
    var delegate: detailViewControllerProtocol?
    var modelObject1 = DetailViewModel()
    var modelObject2 = BookViewModel()
    
    var audioModelObject = AudioPlayer()
    var i = 0
    
    func getDataFromController(row: Int, indexKey: Int) -> [String]
    {
        let data = modelObject1.sendDataToController(arabicKey: indexKey, row: row)
        return data
    }
    
    
    func setAudioPlayer(isPlayerPlaying : Bool, Jump: Int)
    {
        i = 0
        DetailView.detailPlayerPlaying = true
        audioModelObject.myDelegate = self
        var data = modelObject2.getAudioData()//modelObject2.getAudioList()
        print(AudioPlayer.audioNumber)
        
        if(AudioPlayer.audioNumber < data.count-1)
        {
            audioModelObject.updateAudioPlayer(isPlayerPlaying: isPlayerPlaying, data: data, jump: Jump)
        }
    }
    
    func stopAudioPlayer()
    {
        audioModelObject.stopPlayer()
    }
    
    func moveForward()
    {
        let data = modelObject2.getAudioData()
            let countt = data.count
        delegate?.moveForward(listCount: countt)
    }
    
    func moveBackward()
    {
        delegate?.moveBackward()
    }
    
    func scrollTableView()
    {
        if(i > 0)
        {
            print("scroll")
            let data = modelObject2.getAudioData()
            let countt = data.count
            delegate?.moveForward(listCount: countt)
        }
        i = i + 1
    }
    
    func changeIcon() {
        
    }
    func updatePlayerIcon() {
        delegate?.updatePlayer()
    }
    
    
}
