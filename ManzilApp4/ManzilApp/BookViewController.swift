//
//  BookViewController.swift
//  ManzilApp
//
//  Created by Admin on 7/11/17.
//  Copyright © 2017 Mujadidia. All rights reserved.
//
protocol bookViewControllerDelegate
{
    func moveTable(doScroll: Bool)
    func setPlayerIcon()
    func updatePlayer()
}

import Foundation

class BookViewController: AudioClassProtocol
{
    var modelObject = BookViewModel()
    var audioModelObject = AudioPlayer()
    var appData = MainApplicationData()
    var delegate : bookViewControllerDelegate!
    var delegate1 : bookViewControllerDelegate!
    
    static var audioFavorite = 0
    static var audioFavorite2 = 0
    
    var coreDataObject = AppDataModel()
    
    
    func getData() -> [AppDataStruct]
    {
        var data = modelObject.getBookData()
        return data
    }
    
    func setAudioPlayer(isPlayerPlaying : Bool, Jump: Int)
    {
        print(AudioPlayer.audioNumber)
        audioModelObject.myDelegate = self

        var data = modelObject.getAudioData()
        print(data)
        audioModelObject.setBackgroundMusic()
        audioModelObject.setRouteChange()
        audioModelObject.setLockScreenButtons(audios: data)
        audioModelObject.updateAudioPlayer(isPlayerPlaying: isPlayerPlaying, data: data, jump: Jump)
    }
    
    func scrollTableView()
    {
        delegate?.moveTable(doScroll: true)
    }

    func moveForward()
    {
        print("a = \(AudioPlayer.audioNumber)")
        audioModelObject.audioList = modelObject.getAudioData()//modelObject.getAudioList()
        var doScroll = audioModelObject.moveForward()
        print(doScroll)
        delegate?.moveTable(doScroll: doScroll)
    }
    
    func moveBackward()
    {
        print("a = \(AudioPlayer.audioNumber)")
        var doScroll = audioModelObject.moveBackward()
        delegate?.moveTable(doScroll: doScroll)
    }
    
      
    func stopAudioPlayer()
    {
        audioModelObject.stopPlayer()
    }
    
    func changeIcon() {
        delegate.setPlayerIcon()
    }
    
    func updatePlayerIcon()
    {
        delegate.updatePlayer()
    }
    
}
