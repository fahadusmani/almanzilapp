//
//  SideMenuDrawerTableCell.swift
//  ManzilApp
//
//  Created by Admin on 8/1/17.
//  Copyright © 2017 Mujadidia. All rights reserved.
//
protocol SideMenuDrawerCellProtocol
{
    func audioSegmentConroller(cell: SideMenuDrawerTableCell)
    func fontCellSegmentConroller(cell: SideMenuDrawerTableCell)
}

import UIKit

class SideMenuDrawerTableCell: UITableViewCell {
    
    var delegate : SideMenuDrawerCellProtocol?
    
    @IBOutlet weak var audioCellImageView: UIImageView!
    @IBOutlet weak var audioCellLabel: UILabel!
    @IBOutlet weak var audioCellSegmentController: UISegmentedControl!
    
    @IBOutlet weak var fontCellImageView: UIImageView!
    @IBOutlet weak var fontCellLabel: UILabel!
    @IBOutlet weak var fontCellSegmentController: UISegmentedControl!
    
    @IBOutlet weak var drawerCellImageView: UIImageView!
    @IBOutlet weak var drawerCellLabel: UILabel!
    
    
    
    @IBAction func audioCellSegmentController(_ sender: Any)
    {
        if let delegate = delegate
        {
            delegate.audioSegmentConroller(cell: self)
        }
        
    }
    
    @IBAction func fontCellSegmentController(_ sender: Any)
    {
        if let delegate = delegate
        {
            delegate.fontCellSegmentConroller(cell: self)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
