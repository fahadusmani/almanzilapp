//
//  MainVcFunctions.swift
//  ManzilApp
//
//  Created by Admin on 8/22/17.
//  Copyright © 2017 Mujadidia. All rights reserved.
//

import Foundation

class MainVcFunctions
{
    
    
    func setNavBar(mainView: UIView, navBar: UINavigationController, background: UIView, sideButtonItem: UIBarButtonItem)
    {
        navBar.navigationBar.setTitleVerticalPositionAdjustment(CGFloat(-10), for: UIBarMetrics.default)
        
        
        navBar.navigationBar.topItem?.title = "Al-Manzil | Al-Ruqyah Al-Shariah"
        navBar.navigationBar.setBackgroundImage(#imageLiteral(resourceName: "navBarBackground"), for: UIBarMetrics.default)
        navBar.navigationBar.barTintColor = Colors.cardViewBackgroundColor
        navBar.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:  Colors.cardViewBackgroundColor]
        background.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height * 0.11)
        background.backgroundColor = Colors.themeColor.withAlphaComponent(0.9)
        sideButtonItem.tintColor = Colors.cardViewBackgroundColor
    }
    
    
    
    func setView(view: UIView, table: UITableView, segment: UISegmentedControl)
    {
        view.backgroundColor = Colors.mainViewBackgroundColor
        
        table.backgroundColor = Colors.mainViewBackgroundColor
        var y =  view.frame.height * 0.20
        var h = view.frame.height * 0.74
        var frame = CGRect(x: CGFloat(0), y: CGFloat(y), width: view.frame.width, height: CGFloat(h))
        table.frame = frame
        table.backgroundColor = Colors.mainViewBackgroundColor
        
        
        segment.tintColor = Colors.themeColor
        segment.backgroundColor = Colors.cardViewBackgroundColor
        var y1 = view.frame.height * 0.125
        var h1 = view.frame.height * 0.06
        var x1 = view.frame.width * 0.04
        var w1 = view.frame.width * 0.92
        var frame1 = CGRect(x: CGFloat(x1), y: CGFloat(y1), width:w1, height: CGFloat(h1))
        segment.frame = frame1
        segment.tintColor = Colors.themeColor
        
        let attr = NSDictionary(object: UIFont(name: "NotoSans", size: 14.0)!, forKey: NSFontAttributeName as NSCopying)
        UISegmentedControl.appearance().setTitleTextAttributes(attr as [NSObject : AnyObject] , for: .normal)
    }
    
    
    func setPlayerView(mainView: UIView, view: UIView, button1: UIButton, button3: UIButton, button2: UIButton)
    {
        var y = mainView.frame.height * 0.93
        var h = mainView.frame.height * 0.07
        var x = 0
        var w = mainView.frame.width
        var frame = CGRect(x: CGFloat(x), y: CGFloat(y), width:w, height: CGFloat(h))
        view.frame = frame
        view.backgroundColor = Colors.themeColor
        
        button1.frame = CGRect(x: view.frame.width * 0.17, y: 0, width: view.frame.width * 0.22, height: view.frame.height)
        
        button1.tintColor = Colors.cellTextColor
        
        button2.frame = CGRect(x: view.frame.width * 0.39, y: 0, width: view.frame.width * 0.22, height: view.frame.height)
        button2.tintColor = Colors.cellTextColor
        
        button3.frame = CGRect(x: view.frame.width * 0.61, y: 0, width: view.frame.width * 0.22, height: view.frame.height)
        button3.tintColor = Colors.cellTextColor
    }
    
    func setCellView(cell: BookViewTableCell) -> BookViewTableCell
    {
        cell.backgroundColor = Colors.mainViewBackgroundColor
        
        cell.backgroundCardView.backgroundColor = Colors.cardViewBackgroundColor
        cell.backgroundCardView.layer.cornerRadius = 5.0
        cell.backgroundCardView.layer.masksToBounds = false
        cell.backgroundCardView.layer.shadowColor = UIColor.black.withAlphaComponent(0.2).cgColor
        cell.backgroundCardView.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        cell.backgroundCardView.layer.shadowOpacity = 1.8
        cell.newLabel.textAlignment = NSTextAlignment.center
        cell.newLabel.font = cell.newLabel.font.withSize(CGFloat(Colors.fontSize))
        return cell
    }
    
    
    
    func getAttributedString(stringValue: String) -> NSAttributedString
    {
        let attrString = NSMutableAttributedString(string: stringValue)
        var style = NSMutableParagraphStyle()
        style.lineSpacing = 16 // change line spacing between paragraph like 36 or 48
        style.minimumLineHeight = 6 // change line spacing between each line like 30 or 40
        
        attrString.addAttribute(NSParagraphStyleAttributeName, value: style, range: NSRange(location: 0, length: stringValue.characters.count))
        
        attrString.enumerateAttribute(NSFontAttributeName, in: NSMakeRange(0, attrString.length), options: []) { value, range, stop in
            guard let currentFont = value as? UIFont else {
                return
            }
        }
        return attrString
    }
    
    
}
