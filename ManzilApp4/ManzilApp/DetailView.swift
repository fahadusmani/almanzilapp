//
//  DetailView.swift
//  ManzilApp
//
//  Created by Admin on 7/11/17.
//  Copyright © 2017 Mujadidia. All rights reserved.
//

import UIKit
import KYDrawerController

class DetailView: UIViewController, UITableViewDelegate, UITableViewDataSource, DetailViewCellProtocol, detailViewControllerProtocol, KYDrawerControllerDelegate
{
    
    // var cellClassObj = detailViewTableCellClass()
    static var audioNumber = 0
    static var detailPlayerPlaying = false
    @IBOutlet weak var detailViewTable: UITableView!
    var controllerObject = DetailViewController()
    var isPlayerPlaying = false
    var temp = 0
    var start = ""
    
    @IBOutlet weak var detailPlayerView: UIView!
    @IBOutlet weak var detailPreviousAudioButton: UIButton!
    @IBOutlet weak var detailPlayAudioButton: UIButton!
    @IBOutlet weak var detailNextAudioButton: UIButton!
    @IBOutlet weak var navBarBackgroundView: UIView!
    
    
    @IBAction func detailPreviousAudioButton(_ sender: Any)
    {
        if(isPlayerPlaying)
        {
            controllerObject.setAudioPlayer(isPlayerPlaying: isPlayerPlaying, Jump: 1)
            controllerObject.moveBackward()
        }
        else
        {
            controllerObject.moveBackward()
        }
        
    }
    
    @IBAction func detailPlayAudioButton(_ sender: Any) {
        
        if(isPlayerPlaying)
        {
            //DetailView.detailPlayerPlaying = false
            controllerObject.setAudioPlayer(isPlayerPlaying: isPlayerPlaying, Jump: 0)
            isPlayerPlaying = false
            detailPlayAudioButton.setImage(#imageLiteral(resourceName: "play-button"), for: .normal)
        }
        else
        {
            //DetailView.detailPlayerPlaying = true
            AudioPlayer.audioNumber = index! - 1
            controllerObject.setAudioPlayer(isPlayerPlaying: isPlayerPlaying, Jump: 0)
            isPlayerPlaying = true
            detailPlayAudioButton.setImage(UIImage(named: "pause.png"), for: .normal)
        }
        detailViewTable.reloadData()
    }
    
    
    @IBAction func detailNextAudioButton(_ sender: Any) {
        if(isPlayerPlaying)
        {
            controllerObject.setAudioPlayer(isPlayerPlaying: isPlayerPlaying, Jump: 2)
            controllerObject.moveForward()
        }
        else
        {
            controllerObject.moveForward()
        }
        
    }
    
    var _index : Int?
    var index : Int?
    {
        get{ return _index! }
        set{ index = _index! }
    }
    
    
    
    var _detailData : [AppDataStruct]?
    var detailData : [AppDataStruct]?
    {
        get{ return _detailData! }
        set{ detailData = _detailData! }
    }
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        if let drawerController = navigationController?.parent as? KYDrawerController
        {
            drawerController.screenEdgePanGestureEnabled = false
        }
        
        setDetailTableViewFrame()
        detailViewTable.estimatedRowHeight = 200
        detailViewTable.rowHeight = UITableViewAutomaticDimension
        setDetailPlayerViewFrame()
        setPlayerButtonsFrame()
        temp = AudioPlayer.audioNumber
        AudioPlayer.audioNumber = (detailData?[index!].id)! //key!
        controllerObject.delegate = self
        self.view.backgroundColor = Colors.mainViewBackgroundColor
        self.detailViewTable.backgroundColor = Colors.cardViewBackgroundColor
        
        self.navBarBackgroundView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height * 0.11)
        self.navBarBackgroundView.backgroundColor = Colors.themeColor.withAlphaComponent(0.9)
        
        self.navigationController?.navigationBar.barTintColor = Colors.cardViewBackgroundColor
        
        //self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: Colors.cardViewBackgroundColor]
        if(MainVc.bookNumber == 1)
        {
            self.title = "Al-Manzil"
            start = (detailData?[1].arabic)!
        }
        else
        {
            self.title = "Al-Ruqyah Al-Shariah"
            start = (detailData?[2].arabic)!
        }
        
        self.navigationController?.navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont(name: "NotoSans-Bold", size: UIScreen.main.bounds.width * 0.048),                                                                                                    NSForegroundColorAttributeName : Colors.mainViewBackgroundColor
        ]
        
        self.navigationItem.hidesBackButton = true
        let newBackButton = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.back(sender:)))
        newBackButton.image = #imageLiteral(resourceName: "back_btn")
        
        newBackButton.setBackgroundVerticalPositionAdjustment(-8, for: .default)
        
        self.navigationItem.leftBarButtonItem = newBackButton
        
        //setBackgroundVerticalPositionAdjustment(CGFloat(-10), for: .default)
        
        detailViewTable.tableFooterView = UIView(frame: .zero)
        
        self.detailViewTable.reloadData()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        UIView.transition(with: detailViewTable,
                          duration:  0.35,
                          options: .transitionCrossDissolve,
                          animations:
            { () -> Void in
                self.detailViewTable.reloadData()
        },
                          completion: nil);
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        if(isPlayerPlaying)
        {
            controllerObject.stopAudioPlayer()
        }
        AudioPlayer.audioNumber = temp
        
        if let drawerController = navigationController?.parent as? KYDrawerController
        {
            drawerController.screenEdgePanGestureEnabled = true
        }
        
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 8
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //  if(indexPath.row == 0 || indexPath.row == 2)
        //  {
        
        
        if(indexPath.row == 0 || indexPath.row == 2 || indexPath.row == 4 ||  indexPath.row == 6)
        {
            var cell = tableView.dequeueReusableCell(withIdentifier: "cell2", for: indexPath) as! detailTableCell
            
            cell.delegate = self
            let font = UIFont(name: "NotoSans", size: 18)
            cell.customTitleLabel.font = font
            
            switch indexPath.row {
                
            case 0:
                cell.backgroundColor = Colors.detailTitleBackGroundColor
                cell.customTitleLabel.backgroundColor = Colors.detailTitleBackGroundColor
                cell.customTitleLabel.textColor = Colors.themeColor
                cell.customTitleLabel.text = "     Arabic"
                
                
            case 2:
                cell.backgroundColor = Colors.detailTitleBackGroundColor
                cell.customTitleLabel.backgroundColor = Colors.detailTitleBackGroundColor
                cell.customTitleLabel.textColor = Colors.themeColor
                cell.customTitleLabel.text = "     Transliteration"
                
            case 4:
                cell.backgroundColor = Colors.detailTitleBackGroundColor
                cell.customTitleLabel.backgroundColor = Colors.detailTitleBackGroundColor
                cell.customTitleLabel.textColor = Colors.themeColor
                cell.customTitleLabel.text = "     Translation"
                
            case 6:
                if(detailData?[index!].arabic! == start || index! == 1)
                {
                    cell.isHidden = true
                }
                else
                {
                    cell.backgroundColor = Colors.detailTitleBackGroundColor
                    cell.customTitleLabel.backgroundColor = Colors.detailTitleBackGroundColor
                    cell.customTitleLabel.textColor = Colors.themeColor
                    cell.customTitleLabel.text = "     Reference"
                }
                
            default:
                break
            }
            
            return cell
        }
            
        else
        {
            var cell = tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as! detailTableCell
            
            cell.delegate = self
            cell.customLabel.numberOfLines = 0
            cell.customLabel.lineBreakMode = .byWordWrapping
            
            
            switch indexPath.row
            {
                
            case 1:
                cell.customLabel.text = detailData?[index!].arabic!
                
                cell.customLabel.textAlignment = NSTextAlignment.center
                let font = UIFont(name: "noorehuda", size: CGFloat(Colors.fontSize))
                cell.customLabel.font = font
                
            case 3:
                cell.customLabel.text = detailData?[index!].roman!
                cell.customLabel.textAlignment = .left
                let font = UIFont(name: "NotoSans", size: CGFloat(Colors.fontSize/2))
                cell.customLabel.font = font
                
            case 5:
                cell.customLabel.text = detailData?[index!].translation!
                cell.customLabel.textAlignment = .left
                
                let font = UIFont(name: "NotoSans", size: CGFloat(Colors.fontSize/2))
                cell.customLabel.font = font
                
            case 7:
                if(detailData?[index!].arabic! == start)
                {
                    cell.isHidden = true
                }
                else
                {
                    cell.customLabel.text = detailData?[index!].reference!
                    cell.customLabel.textAlignment = .left
                    
                    let font = UIFont(name: "NotoSans", size: CGFloat(Colors.fontSize/2))
                    cell.customLabel.font = font
                }
            default:
                break
            }
            
            setDetailViewTableCellFrame(cell: cell)
            return cell
        }
    }
    
    
    
    func setDetailTableViewFrame()
    {
        var y =  view.frame.height * 0.11
        var h = view.frame.height * 0.82
        var frame = CGRect(x: CGFloat(0), y: CGFloat(y), width: view.frame.width, height: CGFloat(h))
        detailViewTable.frame = frame
        
        // detailViewTable.backgroundColor = UIColor.lightGray
    }
    
    func setDetailViewTableCellFrame(cell: UITableViewCell)
    {
        cell.frame.size.width = detailViewTable.frame.width
        cell.backgroundColor = Colors.cardViewBackgroundColor
    }
    
    func setDetailPlayerViewFrame()
    {
        var y = view.frame.height * 0.93
        var h = view.frame.height * 0.07
        var x = 0
        var w = view.frame.width
        var frame = CGRect(x: CGFloat(x), y: CGFloat(y), width:w, height: CGFloat(h))
        detailPlayerView.frame = frame
        detailPlayerView.backgroundColor = Colors.themeColor
    }
    
    func setPlayerButtonsFrame()
    {
        detailPreviousAudioButton.frame = CGRect(x: detailPlayerView.frame.width * 0.17, y: 0, width: detailPlayerView.frame.width * 0.22, height: detailPlayerView.frame.height)
        
        detailPreviousAudioButton.tintColor = Colors.cellTextColor
        
        detailPlayAudioButton.frame = CGRect(x: detailPlayerView.frame.width * 0.39, y: 0, width: detailPlayerView.frame.width * 0.22, height: detailPlayerView.frame.height)
        detailPlayAudioButton.tintColor = Colors.cellTextColor
        
        detailNextAudioButton.frame = CGRect(x: detailPlayerView.frame.width * 0.61, y: 0, width: detailPlayerView.frame.width * 0.22, height: detailPlayerView.frame.height)
        detailNextAudioButton.tintColor = Colors.cellTextColor
        
    }
    
    
    func moveForward(listCount: Int)
    {
        if(_index! < listCount - 1)
        {
            _index = _index! + 1
            viewWillAppear(true)
            detailViewTable.reloadData()
        }
    }
    
    func moveBackward() {
        if(_index! > 1)
        {
            _index = _index! - 1
            viewWillAppear(true)
            detailViewTable.reloadData()
        }
    }
    
    func updatePlayer()
    {
        isPlayerPlaying = false
        detailPlayAudioButton.setImage(#imageLiteral(resourceName: "play-button"), for: .normal)
    }
    
    
    func getAttributedString(stringValue: String) -> NSMutableAttributedString
    {
        let attrString = NSMutableAttributedString(string: stringValue)
        var style = NSMutableParagraphStyle()
        style.lineSpacing = 16 // change line spacing between paragraph like 36 or 48
        style.minimumLineHeight = 6 // change line spacing between each line like 30 or 40
        
        attrString.addAttribute(NSParagraphStyleAttributeName, value: style, range: NSRange(location: 0, length: stringValue.characters.count))
        
        attrString.enumerateAttribute(NSFontAttributeName, in: NSMakeRange(0, attrString.length), options: []) { value, range, stop in
            guard let currentFont = value as? UIFont else {
                return
            }
        }
        return attrString
    }
    
    
    func back(sender: UIBarButtonItem)
    {
        _ = navigationController?.popViewController(animated: true)
    }
    
    
    
}

